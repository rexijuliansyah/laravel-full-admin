<?php

namespace App\Exports;

use App\Models\Menu;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithCustomStartCell;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Maatwebsite\Excel\Concerns\ShouldAutoSize;



class MenuExport implements FromCollection, WithCustomStartCell, WithHeadings, ShouldAutoSize
{

    public function __construct($menu_name = null, $menu_type = null )
    {
        $this->menu_name = $menu_name;
        $this->menu_type = $menu_type;

    }

    public function collection()
    {
        $params = [];
        if ($this->menu_name) {
            array_push($params, ['MENU_NAME', 'like', '%' . $this->menu_name . '%']) ;
        }

        if ($this->menu_type) {
            if ($this->menu_type == 1) {
                return Menu::query()
                ->select('MENU_ID', 'MENU_NAME', 'MENU_URL', 'MENU_ICON', 'PARENT_ID', 'SEQUENCE')
                ->where($params)
                ->where('PARENT_ID', '=', "0")
                ->orderBy('SEQUENCE')
                ->get();
            } else {
                return Menu::query()
                ->select('MENU_ID', 'MENU_NAME', 'MENU_URL', 'MENU_ICON', 'PARENT_ID', 'SEQUENCE')
                ->where($params)
                ->where('PARENT_ID', '!=', "0")
                ->orderBy('SEQUENCE')
                ->get();
            }
        } else {
            return Menu::query()
            ->select('MENU_ID', 'MENU_NAME', 'MENU_URL', 'MENU_ICON', 'PARENT_ID', 'SEQUENCE')
            ->where($params)
            ->orderBy('UPDATED_DATE','DESC')
            ->get();
        }

    }

    public function startCell(): string
    {
        return 'A1';
    }

    public function headings(): array
    {
        return ["Menu ID", "Menu Name", "Menu URL", "Menu Icon", "Menu Parent", "Sequence"];
    }

}
